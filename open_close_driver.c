#include <linux/module.h>
#include <linux/init.h> // need this header file for the key words __init and __exit which help the kernel to better manage storage resources
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/device.h>

static int major_nr; // the variable where the major number will be written to
static char driver_name[] = "openclose";
static struct cdev *driver_object;
struct class *driver_class;
static struct device *dev;

/*
* Application calls open, then kernel checks, if access allowed, then calls this function.
* This function can also be left out, then access to the driver is always allowed,
* as if the function would only return 0 (like in this example)
*
* @param dev_file contains elements to specify the device file like its owner and access rights
* @param driver_instance contains elements that specify the calling driver instance: access mode, blocking or non-blocking
*/
static int driver_open(struct inode *dev_file, struct file *driver_instance) {
	pr_info("%s: opened", driver_name);
	return 0;
}

static int driver_close(struct inode *dev_file, struct file *driver_instance) {
	pr_info("%s: closed", driver_name);
	return 0;
}

/* Provides the driver methods which were defined above */
static struct file_operations fops = {
	.owner = THIS_MODULE,
	.open = driver_open,
	.release = driver_close,
};

static int automatic_major_nr_register(void) {
	if ((major_nr = register_chrdev(0, driver_name, &fops))) {
		pr_debug("%s: got major number %i\n", driver_name, major_nr);
		return 0; // success
	}
	pr_err("%s: couldn't get major number\n", driver_name);
	return -EIO; // registering failed
}

static int manual_major_nr_register(void) {
	int possible_major_nrs[] = {
		60, 61, 62, 63,
		120, 121, 122, 123, 124, 125, 126, 127,
		240, 241, 242, 243, 244, 245, 246, 247, 248, 249, 250, 251, 252, 253, 254
	};
	// sizeof only gives nr of allocated bytes
	// to get nr of integers in array, you have to divide the byte count 
	// by the size of an int in bytes
	int array_size = sizeof(possible_major_nrs) / sizeof(int);
	int i;
	for (i = 0; i < array_size; ++i) {
		if ((major_nr = register_chrdev(possible_major_nrs[i], driver_name, &fops))) {
			pr_debug("%s: got major number %i\n", driver_name, major_nr);
			return 0;
		}
	}
	pr_err("%s: couldn't get major number\n", driver_name);
	return -EIO; // this will be returned when none of the possible nrs worked
}

static int create_and_add_driver_object(void) {
	driver_object = cdev_alloc(); /* Anmeldeobjekt reservieren */
	
	if (driver_object == NULL) {
		unregister_chrdev(major_nr, driver_name);
		pr_err("%s: couldn't allocate driver object\n", driver_name);
		return -EIO;
	}
		
	driver_object->owner = THIS_MODULE;
	driver_object->ops = &fops;
	
	if (cdev_add(driver_object, major_nr, 1) != 0) {
		kobject_put(&driver_object->kobj); // dereferences object
		unregister_chrdev(major_nr, driver_name);
		pr_err("%s: could not add device to system", driver_name);
		return -EIO;
	}
	return 0;
}

/* Create class for sysfs entry */
static int create_driver_class(void) {
	driver_class = class_create(THIS_MODULE, driver_name);
	if (IS_ERR(driver_class)) {
		pr_err("%s: no udev support\n", driver_name);
		kobject_put(&driver_object->kobj); // dereferences object
		unregister_chrdev(major_nr, driver_name);
		return -EIO;
	}
	return 0;
}

/* 
* Gives udev the info to create the sysfs entry
* creates a device with class, parent, a device nr of type dev_t 
* (data type for device numbers), data for callbacks and string for device name
*/
static int create_driver_entry(void) {
	// MKDEV is a makro to create device nr from major and minor nr
	dev = device_create(driver_class, NULL, MKDEV(major_nr, 0), NULL, "%s", driver_name);
	if (IS_ERR(dev)) {
		pr_err("%s: device_create failed\n", driver_name);
		class_destroy(driver_class);
		unregister_chrdev(major_nr, driver_name);
		return -EIO;
	}
	return 0;
}

static int create_dev(void) {
	if (
		create_and_add_driver_object() == 0
		&& create_driver_class() == 0
		&& create_driver_entry() == 0
	) {
		return 0;
	}
	return -EIO;
}

/* Init function. Name is arbitrary. __init is just a hint, can be left out */
static int __init mod_init(void) {
	// manual_major_nr_register
	if (
		automatic_major_nr_register() == 0
		&& create_dev() == 0
	) {
		pr_info("%s: successfully initialised\n", driver_name);
		return 0;
	}
	pr_err("%s: failed to initialise\n", driver_name);
	return -EIO;
}

/* Also arbitrary name */
static void __exit mod_exit(void) {
	// Delete sysfs entry and also the device class
	device_destroy(driver_class, major_nr);
	class_destroy(driver_class);
	
	// Unregister driver
	cdev_del(driver_object);
	unregister_chrdev(major_nr, driver_name);
	pr_info("%s: unregistered\n", driver_name);
}

module_init(mod_init); // a makro that connects the freely chosen init function name to the actual init function name, for module drivers it's init_module
module_exit(mod_exit); // same but for exit, for module drivers it's cleanup_module

/* Metainformation */
MODULE_AUTHOR("Uta Lemke");
MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("A simple driver that only supports open and close.");
MODULE_SUPPORTED_DEVICE("none");
