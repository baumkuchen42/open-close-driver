# Open Close Driver

A simple driver that just supports being opened and closed. Also creates a device `/dev/openclose`.

Basically like [this project](https://gitlab.com/baumkuchen42/open-close-driver-with-device-number), just with major and minor number instead of device number.

# How to run

```sh
git clone https://gitlab.com/baumkuchen42/open-close-driver.git
cd open-close-driver
make
sudo insmod open_close_driver.ko
```

You can open the driver with the `call_the_driver.c` application.

```sh
gcc call_the_driver.c -o call_the_driver
sudo ./call_the_driver
```

This program will sleep for 5 seconds so you can check in the meantime with `lsmod` that the driver is really being opened.

Afterwards, remove the driver with `sudo rmmod open_close_driver`.
