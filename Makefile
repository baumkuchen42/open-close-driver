ifneq ($(KERNELRELEASE),)
obj-m := open_close_driver.o
else
KDIR := /lib/modules/$(shell uname -r)/build
PWD := $(shell pwd)
default:
	$(MAKE) -C $(KDIR) M=$(PWD) modules
endif
